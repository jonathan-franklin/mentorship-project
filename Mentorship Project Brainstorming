Alright, Marty wants me to work on a project until the end of semester for mentorship that pushes my personal development and possible gives me a good piece to show off to SideFX. What should I do....

# Pre-Development
-- Brainstorm
I could make a sound driven animation. I could use abstract imagery. Or not. Growth algorithms? Sound driven growth algorithms? Rigging and animation? VFX? Stylized VFX? I think I want to go down a stylized route. Keep it short. Less than 30 seconds. Probably less than 20. Let's set a hard cap at 20. Setting? Blank abstract space? Forest? Let's give some amount of setting, nothing too extreme though... A unique and alien flora. Let's me use a custom solver, VFX, and incorporate some environment elements. Keeping it outside also gives me more color freedom. Although, actually, I think it would be good to use either a really strong DOF or do a color, abstract, and dynamic background. Yes, I like that. For colors, I'll reference those flower images I posted on discord previously. Sharp contrasts, vibrant, deep colors. What feelings should this project evoke? Wonder, mystery, a feeling of immersion and ambiance. Balance. Rhythm. Creation.

-- Style Brainstorm
What balance between toon shading and realism do I want? I think one big influence factor will be Land of the Lustrous. There is an interesting blend of toony shading and realism, although I recognize a lot of that was hand-done. Still, I want to emulate the grass shader, I want to capture the glass, and I just want to study how the art was accomplished in general. Style and flourish. Land of the Lustrous seems to have pretty simple lighting, "skin" appears to be either directly lit or indirectly lit. There is also a faint outline line. Maybe I'll try to make that the target of my shader building. I should build this shader with the intent of using it for many future projects. Something I can start building style off of, and same for the rest of the FX. Maybe I should also brainstorm my next semester project concurrently so that I'll know what effects I'll want to focus on.
So, this shading effect can probably be easily accomplished with a simple rendering engine, or even a realtime engine, but... I want to leverage the power of an engine like Mantra or Redshift. How can I leverage the power of a raytracer to amplify my work rather than detract from it. There ought to be a good reason to use a raytracer, because otherwise it's just a waste of rendering time. I should leverage advanced refraction, as well as AOVs.  

-- Wants
* Stylized VFX 
  * Traveling lines and diamonds.
* Color and mood practice
* Custom growth solver
  * Reaction diffusion
  * Quaddel algorithms
* Variations in speed. A combination of slow growth and fast activity.
NOTE: May cut the growth solver bit and focus purely on the vfx.

-- To-Do
* Find whitepapers
* Make a list of FX needed
* Concept sketches
* Do solver tests, follow some Entagma tutorials
* Pick color palettes
  > Assemble in Affinity Photo
* Do POP tests
* Send proposal to Marty

-- Reference
http://edwardfx.weebly.com/uploads/9/0/7/5/9075896/innovationsreport.pdf

# FX List
* Diamond ice effect
* Fire effect
* Impact effect
* Speed effect
* Custom shader
* Expanding concentric circles
* Epiphany effect
* Esper FX
* Shatter/Radiative Effect