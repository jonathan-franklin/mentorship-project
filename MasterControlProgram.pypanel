<?xml version="1.0" encoding="UTF-8"?>
<pythonPanelDocument>
  <!-- This file contains definitions of Python interfaces and the
 interfaces menu.  It should not be hand-edited when it is being
 used by the application.  Note, that two definitions of the
 same interface or of the interfaces menu are not allowed
 in a single file. -->
  <interface name="rigging_controller" label="Rigging Controller" icon="MISC_python" showNetworkNavigationBar="false" help_url="">
    <script><![CDATA[#
# RIGGING MASTER CONTROL PROGRAM
#

from PySide2 import QtWidgets
import toolutils
import math

def parentAndKeepPos(obj_node, new_parent):
    xform = obj_node.worldTransform()
    obj_node.setFirstInput(new_parent)
    obj_node.setWorldTransform(xform)

def createInterface():

    layout = QtWidgets.QVBoxLayout()
    obj = hou.node("/obj")
    
    # ----------------------------------------------#
    # Renamer
    
    rename_title = QtWidgets.QLabel("Renamer")
    nname_label = QtWidgets.QLabel("New Name")
    padding_label = QtWidgets.QLabel("Padding")
    rframe = QtWidgets.QFrame()
    
    # Fields
    new_name = QtWidgets.QLineEdit()
    padding = QtWidgets.QLineEdit()
    # Execute Button
    rname_b1 = QtWidgets.QPushButton("Execute")
    
    def renamer_wrapper():
        renamer(new_name.text(), int(padding.text()))
    
    def renamer(rename,padding):
        sel = hou.selectedNodes()  
        holding = rename.split('#')
        i = 1
        
        for node in sel:
            if padding == 1:
                newSel = holding[0] + str(i) + holding[1]
                hou.node("obj/" + str(node)).setName(newSel, 1)
                i += 1
            elif padding == 2 and i < 10:
                newSel = holding[0] + "0" + str(i) + holding[1]
                hou.node("obj/" + str(node)).setName(newSel, 1)
                i += 1
            elif padding == 3 and i < 10:
                newSel = holding[0] + "00" + str(i) + holding[1]
                hou.node("obj/" + str(node)).setName(newSel, 1)
                i += 1
            elif padding == 3 and i < 100:
                newSel = holding[0] + "0" + str(i) + holding[1]
                hou.node("obj/" + str(node)).setName(newSel, 1)
                i += 1
            elif padding == 4 and i < 10:
                newSel = holding[0] + "000" + str(i) + holding[1]
                hou.node("obj/" + str(node)).setName(newSel, 1)
                i += 1
            elif padding == 4 and i < 100:
                newSel = holding[0] + "00" + str(i) + holding[1]
                hou.node("obj/" + str(node)).setName(newSel, 1)
                i += 1
            elif padding == 4 and i < 1000:
                newSel = holding[0] + "0" + str(i) + holding[1]
                hou.node("obj/" + str(node)).setName(newSel, 1)
                i += 1
            else:
                newSel = holding[0] + "0" + str(i) + holding[1]
                hou.node("obj/" + str(node)).setName(newSel, 1)
                i += 1

    rname_b1.clicked.connect(renamer_wrapper)
    
    rframe.setFrameShape(QtWidgets.QFrame.HLine)
    rframe.setFrameShadow(QtWidgets.QFrame.Plain)
    
    rbox = QtWidgets.QFormLayout()
    rbox.addRow(rename_title)
    rbox.addRow(nname_label,new_name)
    rbox.addRow(padding_label,padding)
    rbox.addRow(rname_b1)
    
    layout.addLayout(rbox)
    layout.addWidget(rframe)
    # ----------------------------------------------#
    # Locator Creator
    
    loc_b1 = QtWidgets.QPushButton("Create Locator(Pivot)")
    loc_b2 = QtWidgets.QPushButton("Create Locator(Bounding Box)")
    
    mirrorX = QtWidgets.QCheckBox("X")
    mirrorY = QtWidgets.QCheckBox("Y")
    mirrorZ = QtWidgets.QCheckBox("Z")
    mirror_b1 = QtWidgets.QPushButton("Mirror")
    lframe = QtWidgets.QFrame()
    
    lframe.setFrameShape(QtWidgets.QFrame.HLine)
    lframe.setFrameShadow(QtWidgets.QFrame.Plain)

    def pivotLocator():
        loc_scene = toolutils.sceneViewer()
        print loc_scene
        selection = loc_scene.selectGeometry()
        type = selection.geometryType()
        
        newNode = obj.createNode("geo", str("merged_geo"))
        #Delete the build in File node of the new Geo
        newNode.children()[0].destroy()
        #Create the hero merge node
        oMergeNode = newNode.createNode("merge")
        
        selectionGeo = selection.mergedNode()
        print selectionGeo
        bbox = selectionGeo.boundingBox()
        bbox_center = bbox.center()
        print bbox_center
        #obj.createNode("null")
        
    def mirror():
        selection = hou.selectedNodes()
        for i, mnode in enumerate(selection):
            mirroredNode = obj.createNode("null")
            
            # Mirror X if X is checked
            if mirrorX.isChecked() == 1:
                tx = mnode.parm("tx").eval() * -1
            else:
                tx = mnode.parm("tx").eval()
            # Mirror Y if Y is checked
            if mirrorY.isChecked() == 1:
                ty = mnode.parm("ty").eval() * -1
            else:
                ty = mnode.parm("ty").eval()
            # Mirror Z if Z is checked
            if mirrorZ.isChecked() == 1:
                tz = mnode.parm("tz").eval() * -1
            else:
                tz = mnode.parm("tz").eval()
            # Set transform values
            mirroredNode.parm("tx").set(tx)
            mirroredNode.parm("ty").set(ty)
            mirroredNode.parm("tz").set(tz)
            
    loc_b1.clicked.connect(pivotLocator)
    mirror_b1.clicked.connect(mirror)
    
    lbox = QtWidgets.QGridLayout()
    mirrorCheckboxes = QtWidgets.QGridLayout()
    mirrorCheckboxes.addWidget(mirrorX, 0, 0)
    mirrorCheckboxes.addWidget(mirrorY, 0, 1)
    mirrorCheckboxes.addWidget(mirrorZ, 0, 2)
    mirrorCheckboxes.addWidget(mirror_b1, 0, 3)
    lbox.addWidget(loc_b1, 1, 0)
    lbox.addWidget(loc_b2, 1, 1)
    lbox.addLayout(mirrorCheckboxes, 0, 0)
        
    layout.addStretch()
    layout.addLayout(lbox)
    layout.addWidget(lframe)
    
    # ----------------------------------------------#
    # Joint Creator
    
    joint_b1 = QtWidgets.QPushButton("Create FK Chain")
    
    def joint_b1_pressed():
        createBones()
    
    def createBones():
        sel = hou.selectedNodes()
        # Create root null
        #root = obj.createNode("null", "root")
        bone_list = []
        bone_roots = sel[:-1]
        control_list = []
        
        for i, loc in enumerate(bone_roots):
            root_pos = loc.origin()
            holding = loc.name().split("Loc")
            
            np_bone_vector = -(sel[i+1].origin() - sel[i].origin())
            bone_vector = hou.Vector3(np_bone_vector[0],np_bone_vector[1],np_bone_vector[2])
            n_bone_vector = bone_vector.normalized()
            bone_length = bone_vector.length()
            z_vector = hou.Vector3([0,0,bone_length])
            n_z_vector = z_vector.normalized()
            
            # Create Rotation Quaternion
            q = hou.Quaternion();
            q.setToVectors(n_z_vector, n_bone_vector)
            # Convert Quaternion to Euler Angles
            rotation = q.extractEulerRotates("xyz")
            rotMatrix = q.extractRotationMatrix()
            
            #q2 = hou.Quaternion();
            #q2.setToAngleAxis(90, np_bone_vector)
            #localZRot = q2.extractEulerRotates("xyz")
            
            # Create Bones --------------------------------
            bone = obj.createNode("bone", holding[0] + "Bone")
            bone_list.append(bone)   # Add bone to bone list
            bone.move([1,-2*i])
            
            # Set bone rotation order and length
            bone.parm("rOrd").set(0)
            bone.parm("length").set(bone_length)
            # Set bone rotation
            bone.parm("rx").set(rotation[0])# + localZRot[0])
            bone.parm("ry").set(rotation[1])# + localZRot[1])
            bone.parm("rz").set(rotation[2])# + localZRot[2])
            # Set root translation
            for j, parm in enumerate(['tx', 'ty', 'tz']):
                bone.parm(parm).set(root_pos[j])
            
            # Create Controls -----------------------------
            control = obj.createNode("null", holding[0] + "Ctrl")
            control_list.append(control)
            controlChildren = control.children()
            shape = controlChildren[0]
            control.move([-1, -2*i+1])
            
            # Set control rotation order and length
            control.parm("rOrd").set(0)
            # Set control rotation
            control.parm("rx").set(rotation[0])# + localZRot[0])
            control.parm("ry").set(rotation[1])# + localZRot[1])
            control.parm("rz").set(rotation[2])# + localZRot[2])
            # Set control translation
            for k, parm in enumerate(['tx', 'ty', 'tz']):
                control.parm(parm).set(root_pos[k])
            
            # Set control shape parameters
            shape.parm("controltype").set("circles")
            shape.parm("orientation").set("z")
            shape.parm("scale").set(2)
                
            
            parentAndKeepPos(bone, control)
            if i > 0:
                parentAndKeepPos(control, bone_list[i-1])       
            
    joint_b1.clicked.connect(joint_b1_pressed)
    
    jbox = QtWidgets.QVBoxLayout()
    jbox.addWidget(joint_b1)
    
    layout.addStretch()
    layout.addLayout(jbox)
    
    # ----------------------------------------------#
    # Control Parameters
    
    color = QtWidgets.QColorDialog()
    control_b1 = QtWidgets.QPushButton("Choose color")
    control_b2 = QtWidgets.QPushButton("Set control color")
    control_b3 = QtWidgets.QPushButton("Create Controls")
    
    def setColor():
        control = hou.selectedNodes()
        for c in control:
            c.parm("dcolorr").set(0.0)
            c.parm("dcolorg").set(0.0)
            c.parm("dcolorb").set(1.0)
    
    def b1_press():
        color.getColor()
    def b2_press():
        setColor()
    def b3_press():
        print "Controls created"
    
    control_b1.clicked.connect(b1_press)
    control_b2.clicked.connect(b2_press)
    control_b3.clicked.connect(b3_press)
   
    cbox = QtWidgets.QVBoxLayout()
    conbox = QtWidgets.QHBoxLayout()
    
    conbox.addWidget(control_b1)
    conbox.addWidget(control_b2)
    cbox.addLayout(conbox)
    cbox.addWidget(control_b3)
    
    layout.addStretch()
    layout.addLayout(cbox)
    
    # ---------------------------------------------- #
    
    
    
    # ---------------------------------------------- #
    root_widget = QtWidgets.QWidget()
    root_widget.setLayout(layout)

    
    # Return the top-level widget
    return root_widget]]></script>
    <includeInToolbarMenu menu_position="103" create_separator="false"/>
    <help><![CDATA[]]></help>
  </interface>
</pythonPanelDocument>
